The range of the loop is exclusive of the end point
```yaml
- name: with_sequence -> loop
  ansible.builtin.debug:
    msg: "{{ 'testuser%02x' | format(item) }}"
  loop: "{{ range(0, 4 + 1, 2)|list }}"

# Output:
TASK [with_sequence -> loop] *************************************************************************************************
ok: [localhost] => (item=0) => {
    "msg": "testuser00"
}
ok: [localhost] => (item=2) => {
    "msg": "testuser02"
}
ok: [localhost] => (item=4) => {
    "msg": "testuser04"
}
```
```yaml
- name: Get Interfaces
   debug:
     msg:
     - "{{ hostvars[inventory_hostname]['ansible_%s'| format(item)]['ipv4']['address'] }}"
   loop: "{{ ansible_interfaces }}"

# Output:
ok: [localhost] => (item=enp7s0) => {
    "msg": [
        "172.16.101.202"
    ]
}
ok: [localhost] => (item=enp7s0) => {
    "msg": [
        "172.16.101.202"
    ]
}
ok: [localhost] => (item=enp1s0) => {
    "msg": [
        "192.168.122.194"
    ]
}

```
