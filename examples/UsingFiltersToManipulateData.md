# Product Filters

The product filter returns the cartesian product of the input iterables. En el siguiente ejemplo en cada iteración añade la cadena "com"
```yaml
"{{ ['foo', 'bar'] | product(['com']) }}"
Output:
ok: [localhost] => {
    "msg": [
        [
            "foo",
            "com"
        ],
        [
            "bar",
            "com"
        ]
    ]
}
# Une los contenidos, poniendo como delimitador el "."
"{{ ['foo', 'bar'] | product(['com']) | map('join', '.') }}"
Output:
ok: [localhost] => {
    "msg": [
        "foo.com",
        "bar.com"
    ]
}
# Quita el array para almacenarlo en una unica cadena de string
"{{ ['foo', 'bar'] | product(['com']) | map('join', '.') | join(',') }}"
Output:
{ "msg": "foo.com,bar.com" }
```

# Using selectattr/rejectattr

Very similar to the above but it uses a specific attribute of the list elements for the conditional statement.

Ideal para los siguiente tipos de datos:
```
userdata: [
    {
      "name": "Shanmugam",
      "gender": "male",
      "mobile": "9875643210",
      "dose1completed" : "yes",
      "dose2completed" : "yes",
      "age": "25",
      "city": "pudukottai",
      "state": "Tamilnadu"
    },
    {
      "name": "Lakshmi",
      "gender": "female",
      "mobile": "9875623410",
      "dose1completed" : "yes",
      "dose2completed" : "no",
      "age": "32",
      "city": "Chennai",
      "state": "Tamilnadu"
    },
    {
      "name": "Albert",
      "gender": "male",
      "mobile": "9875634510",
      "dose1completed" : "yes",
      "dose2completed" : "yes",
      "age": "65",
      "city": "Coimbatore",
      "state": "Tamilnadu"
    },
    {
      "name": "Abdul",
      "gender": "male",
      "mobile": "9875632341",
      "dose1completed" : "no",
      "dose2completed" : "no",
      "age": "45",
      "city": "Hosur",
      "state": "Tamilnadu"
    }
]
```



The mount point for {{path}}, found using the Ansible mount facts, [-1] is the same as the 'last' filter
```sql
path: /boot

"{{ansible_facts.mounts | selectattr('mount', 'in', path) | list }}"
# Se obteniene el mismo resultado de la siguiente salida del siguiente filtro

"{{(ansible_facts.mounts | selectattr('mount', 'in', path) | list | sort(attribute='mount'))}}"
Output:
ok: [localhost] => {
    "msg": [
        {
            "block_available": 2787307,
            "block_size": 4096,
            "block_total": 3915776,
            "block_used": 1128469,
            "device": "/dev/mapper/fedora-root",
            "fstype": "xfs",
            "inode_available": 7734966,
            "inode_total": 7864320,
            "inode_used": 129354,
            "mount": "/",
            "options": "rw,seclabel,relatime,attr2,inode64,logbufs=8,logbsize=32k,noquota",
            "size_available": 11416809472,
            "size_total": 16039018496,
            "uuid": "3b544e91-9092-4efd-aec2-6c9088134e94"
        },
        {
            "block_available": 190910,
            "block_size": 4096,
            "block_total": 245760,
            "block_used": 54850,
            "device": "/dev/sda2",
            "fstype": "xfs",
            "inode_available": 523931,
            "inode_total": 524288,
            "inode_used": 357,
            "mount": "/boot",
            "options": "rw,seclabel,relatime,attr2,inode64,logbufs=8,logbsize=32k,noquota",
            "size_available": 781967360,
            "size_total": 1006632960,
            "uuid": "efdb9ba9-5b55-479c-bd61-65813b14d08d"
        }
    ]
}

"{{(ansible_facts.mounts | selectattr('mount', 'in', path) | list | sort(attribute='mount'))[-1]['mount']}}"
Output:
ok: [localhost] => {
    "msg": "/boot"
}
```


```
## File: hosts
localhost ansible_connection=local
ansible-cl1 ansible_host=172.16.100.133

#Selecciona: Verificando que este definido el atributo "ansible_host"
'{{ hostvars|dictsort|selectattr("1.ansible_host", "defined")|map(attribute="1.ansible_host")|list }}'
ó
'{{ hostvars|dictsort|selectattr("1.ansible_host", "defined")|map(attribute="0")|list }}'
TASK [selectattr] 
ok: [localhost] => {
    "msg": [
        "172.16.100.133"
    ]
}

#Rechaza: Verificando que no este definido el atributo "ansible_host"
'{{ hostvars|dictsort|rejectattr("1.ansible_host", "defined")|map(attribute="0")|list }}'
TASK [rejectattr]
ok: [localhost] => {
    "msg": [
        "localhost"
    ]
}
```

Filtrando por el atributo **email**
```
vars:
  users:
    user1:
      name: User1
      email: none
    user2:
      name: User2
      email: user2@example.com
    user3:
      name: User3
      email: none
    user4:
      name: User3
  usersList: "{{ users | list }}"

#Obtener los users que tengan definidos el atributo "email" con valor none
"{{ usersList | map('extract', users ) | selectattr('email', '==' , 'none')  }}"
Output:
ok: [localhost] => {
    "msg": [
        {
            "email": "none",
            "name": "User1"
        },
        {
            "email": "none",
            "name": "User3"
        }
    ]
}

#Obtener los users que tengan definidos el atributo "email"
"{{ usersList | map('extract', users ) | selectattr('email', 'defined')  }}"
Output:
ok: [localhost] => {
    "msg": [
        {
            "email": "none",
            "name": "User1"
        },
        {
            "email": "user2@example.com",
            "name": "User2"
        },
        {
            "email": "none",
            "name": "User3"
        }
    ]
}

#Obtener los users que no tengan definido el atributo "email"
"{{ usersList | map('extract', users ) | selectattr('email', 'undefined')  }}"
Output:
ok: [localhost] => {
    "msg": [
        {
            "name": "User4"
        }
    ]
}
```

# Using the ipaddr filter
Fuente: https://docs.ansible.com/ansible/latest/collections/ansible/utils/docsite/filters_ipaddr.html#topics-local

