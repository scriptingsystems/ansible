```sh
$ ansible-navigator inventory -i inventory -m stdout --graph
@all:
  |--@ungrouped:
  |--@dev:
  |  |--ansible1.ansiblear.com
  |--@qa:
  |  |--ansible2.ansiblear.com
  |--@lb:
  |  |--ansible5.ansiblear.com
  |--@web:
  |  |--@prod:
  |  |  |--ansible3.ansiblear.com
  |  |  |--ansible4.ansiblear.com

```
