1. How to remove empty lines with or without whitespace

testing.yaml
```yaml
...
  - name: Get Content
    template:
      src: /root/.ansible/templates/testing.j2
      dest: /root/.ansible/testing.txt
    vars: 
      listInterfaces: |
        ADDRESS1=192.168.1.2
        NETMASK1=255.255.255.0
        GATEWAY1=192.168.1.254
        ADDRESS2=192.168.2.2
        NETMASK2=255.255.255.0
        GATEWAY2=192.168.2.254
        ADDRESS3=192.168.3.2
        NETMASK3=255.255.255.0
        GATEWAY3=192.168.3.254
        ADDRESS4=192.168.4.2
        NETMASK4=255.255.255.0
        GATEWAY4=192.168.4.254

        ADDRESS5=192.168.5.2
        NETMASK5=255.255.255.0
        GATEWAY5=192.168.5.254

```
testing.j2
```
## Contempla mejor las lineas en blanco
{% for interface in listInterfaces.split('\n') %}
{%   if interface|trim|length > 0 %}
{{     interface }}
{%   endif %}
{% endfor %}
## Unicamente para los casos donde unicamente haya una linea en blanco
{{ listInterfaces | replace('\n\n','\n') }}
```

Output: testing.txt
```bash
ADDRESS1=192.168.1.2
NETMASK1=255.255.255.0
GATEWAY1=192.168.1.254
ADDRESS2=192.168.2.2
NETMASK2=255.255.255.0
GATEWAY2=192.168.2.254
ADDRESS3=192.168.3.2
NETMASK3=255.255.255.0
GATEWAY3=192.168.3.254
ADDRESS4=192.168.4.2
NETMASK4=255.255.255.0
GATEWAY4=192.168.4.254
ADDRESS5=192.168.5.2
NETMASK5=255.255.255.0
GATEWAY5=192.168.5.254
```
