```
"{{ ansible_facts.ens33.features }}"
Output:
"esp_hw_offload": "off [fixed]"
"esp_tx_csum_hw_offload": "off [fixed]"
"fcoe_mtu": "off [fixed]"


"{{ ansible_facts.ens33.features | dict2items }}"
Output:
{
  "key": "esp_hw_offload",
  "value": "off [fixed]"
},
{
  "key": "esp_tx_csum_hw_offload",
  "value": "off [fixed]"
},
{
  "key": "fcoe_mtu",
  "value": "off [fixed]"
}

"{{ ansible_facts.ens33.features | dict2items | selectattr('value', 'match', 'on') }}"
Output:
{
  "key": "tx_checksumming",
  "value": "on"
},
{
  "key": "tx_checksum_ip_generic",
  "value": "on"
},
{
  "key": "scatter_gather",
  "value": "on"
}
```

Convertir cada item del diccionario **users** en un item mas trabable
```
vars:
  users:
    user1:
      name: User1
      email: none
    user2:
      name: User2
      email: user2@example.com
    user3:
      name: User3
      email: none

# Mostramos los items de users
'{{ users }}'
ok: [localhost] => {
    "msg": {
        "user1": {
            "email": "none",
            "name": "User1"
        },
        "user2": {
            "email": "user2@example.com",
            "name": "User2"
        },
        "user3": {
            "email": "none",
            "name": "User3"
        }
    }
}

# Convertimos los users, en items mas trabables
'{{ users| dict2items }}'
ok: [localhost] => {
    "msg": [
        {
            "key": "user1",
            "value": {
                "email": "none",
                "name": "User1"
            }
        },
        {
            "key": "user2",
            "value": {
                "email": "user2@example.com",
                "name": "User2"
            }
        },
        {
            "key": "user3",
            "value": {
                "email": "none",
                "name": "User3"
            }
        }
    ]
}

```
