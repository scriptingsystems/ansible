1. Iterando un array, para crear un formato json con los resultados
```
# File: Playbook
vars:
  users:
  - name: 'Jean Carlos'
    lastname: 'R'
  - name: 'Carlos Angel'
    lastname: 'R'
  - name: 'Irene Del Rosario'
    lastname: 'R'

# File: Template
[
{% for user in users %}
  {
    name: {{  user.name }}
    lastname: {{ user.lastname }}
  }{% if users|length > 1 and loop.index < users|length %},{% endif %}

{% endfor %}
]

# Output
[
  {
    name: Jean Carlos
    lastname: R
  },
  {
    name: Carlos Angel
    lastname: R
  },
  {
    name: Irene Del Rosario
    lastname: R
  }
]
```
