Playbook:
```yaml
  - name: "[Template] Crear el fichero myTemplate.txt"
    vars:
      listColumnNames:
        L1:
          - lvmtsgl1
          - lvmtsgl2
          - lvmtsgl3
          - lvmtsgl4
        M1:
          - lvmtsgm1
          - lvmtsgm2
          - lvmtsgm3
        X1:
          - lvmtsgx1
          - lvmtsgx2
          - lvmtsgx3
          - lvmtsgx4
          - lvmtsgx5
    ansible.builtin.template:
      src: myTemplate.txt.j2
      dest: myTemplate.txt
```

Template:
```jinja2
{#   File templates/myTemplate.txt.j2 #}

{#   Inicializar un objeto, para evitar que se pierda el valor de la variable #}
{%   set ns = namespace() %}
{%   set ns.getListLength = {} %}

{#   Obtener el numero total de elementos de cada variable de la lista #}
{%-  for line in listColumnNames.keys() %}
{%-    set ns.getListLength = ns.getListLength | combine({line: listColumnNames[line]|length}) -%}
{%-  endfor %}

Mostrar el numero de elmentos que tiene cada variable de la lista:
{{ ns.getListLength }}
```


Output:
```bash
Mostrar el numero de elmentos que tiene cada variable de la lista:
{'L1': 4, 'M1': 3, 'X1': 5}
```
