# Pasar Argumentos
1.  k=v format: -a 'opt1=val1 opt2=val2'
```
[root@ansible-s1 .ansible]# ansible localhost -m wait_for -a "host=localhost port=22"
localhost | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "elapsed": 0,
    "match_groupdict": {},
    "match_groups": [],
    "path": null,
    "port": 22,
    "search_regex": null,
    "state": "started"
}
```

2. json string: -a '{"opt1": "val1", "opt2": "val2"}'
```
[root@ansible-s1 .ansible]# ansible localhost -m wait_for -a '{"host": "localhost", "port": "22"}'
localhost | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "elapsed": 0,
    "match_groupdict": {},
    "match_groups": [],
    "path": null,
    "port": 22,
    "search_regex": null,
    "state": "started"
}
```
