# Using map


1. "chains", utilizamos el listado para extraer esos diccionarios de la variable "chains_config", a su vez obtenemos el atributo "configs" de cada diccionario, la funcion flatten mantiene un unico array con todas las entradas

```yaml
---
- name: Tasks
  hosts: localhost
  tasks:
    - name: Show extracted list of keys from a list of dictionaries
      ansible.builtin.debug:
        msg: "{{ chains | map('extract', chains_config) | map(attribute='configs') | flatten | map(attribute='type') | flatten }}"
      vars:
#       chains: [1, 2] # Sustituimos esta linea para calcularlo el listado de entradas de "chains_config"
        chains_config:
            1:
                foo: bar
                configs:
                    - type: routed
                      version: 0.1
                    - type: bridged
                      version: 0.2
            2:
                foo: baz
                configs:
                    - type: routed
                      version: 1.0
                    - type: bridged
                      version: 1.1
        chains: "{{ chains_config | list }}"
Output:
ok: [localhost] => {
    "msg": [
        "routed",
        "bridged",
        "routed",
        "bridged"
    ]
}
```

"chains", utilizamos el listado para extraer esos diccionarios de la variable "chains_config", a su vez obtenemos el atributo "configs" de cada diccionario, la funcion flatten mantiene un unico array con todas las entradas
```yaml
{{ chains | map('extract', chains_config) | map(attribute='configs') | flatten | map(attribute='type') | flatten }}
# otro ejemplo
"{{ [3, [4, 2] ] | flatten }}"
Output:
ok: [localhost] => {
    "msg": [
        3,
        4,
        2
    ]
}
```

2. Extract

Del listado dado "['x','y','z']", extrae unicamente las posiciones dadas "[0,2]"
```yaml
"{{ [0,2] | map('extract', ['x','y','z']) }}"
Output:
ok: [localhost] => {
    "msg": [
        "x",
        "z"
    ]
}
```

Del listado dado "{'x': 42, 'y': 31, 'z': 20}", extrae unicamente las posiciones dadas "['x','y']"
```yaml
"{{ ['x','y'] | map('extract', {'x': 42, 'y': 31, 'z': 20}) | list }}"
Output:
ok: [localhost] => {
    "msg": [
        42,
        31
    ]
}
```

3. get a comma-separated list of the mount points (e.g. "/,/mnt/stuff") on a host
```yaml
"{{ ansible_mounts | map(attribute='mount') }}"
Output:
ok: [localhost] => {
    "msg": [
        "/",
        "/boot"
    ]
}

"{{ ansible_mounts | map(attribute='mount') | join(',') }}"
Output:
ok: [localhost] => {
    "msg": [
        "x",
        "z"
    ]
}
```

4. add "https://" prefix to each item in a list
```
# Teniendo la siguiente variable
hosts: ['webserver1','webserver2','webserver3']
# Aplicamos el siguiente filtro
"{{ hosts | map('regex_replace', '^(.*)$', 'https://\\1') | list }}"

Output:
ok: [localhost] => {
    "msg": [
        "https://webserver1",
        "https://webserver2",
        "https://webserver3"
    ]
}
```

5. The basic usage is mapping on an attribute. Imagine you have a list of **users** but you are only interested in a list of **names**:
```
# playbook
users:
- name: 'Jean Carlos'
  lastname: 'Rojas'
- name: 'Carlos Angel'
  lastname: 'Rojas'

# Template
Users on this page: {{ users | map(attribute='name')|join(',') }}

#Output:
Users on this page: Jean Carlos,Carlos Angel
```

6. Basando en el ejemplo anterior, aplicar un filtro para convertir las letras en minuscula
```
#Playbook:
Users on this page: {{ titles|map('lower')|join(', ') }}

#Output:
Users on this page: ["{'name': 'jean carlos', 'lastname': 'rojas'}", "{'name': 'carlos angel', 'lastname': 'rojas'}"]
```

