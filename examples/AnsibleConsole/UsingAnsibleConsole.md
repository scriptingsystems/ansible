Haciendo uso del modulo **wait_for**
```
[root@ansible-s1 .ansible]# ansible-console localhost
root@localhost (1)[f:5]$ wait_for host=172.16.100.133 port=22
localhost | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "elapsed": 0,
    "match_groupdict": {},
    "match_groups": [],
    "path": null,
    "port": 22,
    "search_regex": null,
    "state": "started"
}
```
