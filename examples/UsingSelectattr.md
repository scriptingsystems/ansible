**Fuente** - Comparation: https://jinja.palletsprojects.com/en/3.0.x/templates/#comparisons



```
vars:
  userdata:
    - "name": "Shanmugam"
      "gender": "male"
      "mobile": "9875643210"
      "dose1completed" : "yes"
      "dose2completed" : "yes"
      "age": "25"
      "city": "pudukottai"
      "state": "Tamilnadu
    - "name": "Lakshmi"
      "gender": "female"
      "mobile": "9875623410"
      "dose1completed" : "yes"
      "dose2completed" : "no"
      "age": "32"
      "city": "Chennai"
      "state": "Tamilnadu
    - "name": "Albert"
      "gender": "male"
      "mobile": "9875634510"
      "dose1completed" : "yes"
      "dose2completed" : "yes"
      "age": "65"
      "city": "Coimbatore"
      "state": "Tamilnadu
    - "name": "Abdul"
      "gender": "male"
      "mobile": "9875632341"
      "dose1completed" : "no"
      "dose2completed" : "no"
      "age": "45"
      "city": "Hosur"
      "state": "Tamilnadu"
...
- agegroup18to45: "{{ userdata | selectattr('age','>=','18') }}"
- dose2pending: "{{ userdata | selectattr('dose2completed','==','no') }}"
- redzoneresidents: "{{ userdata | selectattr('city','in','Coimbatore,Chennai')}}"
```
