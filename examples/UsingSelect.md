Code:
```
  - name: Get orange
    debug:
      msg: "Number orange: {{ listFruits | select('match','^orange$') | list | length }}"
    vars: 
      listFruits: ['orange','apple','banana','kiwi', 'orange1', 'aorange', 'orange']
```

Output:
```
TASK [Get orange] **************************************************************
ok: [localhost] => {
    "msg": "Number orange: 2"
}
```


**selectattr** filter in Ansible is useful for filtering lists based on attributes of the objects in the list
```
selectattr('name', 'match', 'eth[2-9]')
```


Ansible’s **sum** filter can be used for reducing lists into new objects (including lists)
```
sum(attribute='ips', start=[])
```



Example:
```
vars:
  interfaces:
  - gw: 10.10.10.1
    name: eth1
    ips:
    - {ip: 10.10.10.104, owner: TEST, project: The Project}
    - {ip: 10.10.10.105, owner: Our Client, project: The Project}
  - gw: 10.10.10.1
    name: eth2
    ips:
    - {ip: 10.10.10.204, owner: TEST, project: The Project}
    - {ip: 10.10.10.205, owner: Our Client, project: The Project}
    - {ip: 10.10.10.206, owner: Our Client, project: The Project}
    - {ip: 10.10.10.207, owner: Our Client, project: The Project}
    - {ip: 10.10.10.208, owner: Our Client, project: The Project}
    - {ip: 10.10.10.209, owner: Our Client, project: The Project}
tasks:
  - set_fact:
  test_ip: "{{ interfaces 
  | selectattr('name', 'match', 'eth[2-9]') 
  | sum(attribute='ips', start=[]) 
  | selectattr('owner', 'equalto', 'TEST') 
  | map(attribute='ip')
  | list 
  | first 
  | default('NOT_FOUND') }}"
  - debug:
  msg: "The TEST IP is {{ test_ip }}"
 ```

Output:
 ```
TASK [debug] *******************************************************
ok: [localhost] {
 "msg": "The TEST IP is 10.10.10.204"
}
 ```
